import astropy.io.fits as pyfits
import matplotlib.pyplot as plt
import numpy as np
from astropy import units as u
from astropy.coordinates import SkyCoord
import numpy.fft as npf
import scipy.ndimage as spn
import scipy.interpolate as spi
from scipy.interpolate import interp2d

plt.close('all')

# Path and name of the chosen PSF cube
filenames = ['EUC_NIR_W-CALIB-PSF_53402-H-1_20170717T104421.7Z_00.00.fits',
             'EUC_NIR_W-CALIB-PSF_53402-J-1_20170712T235537.2Z_00.00.fits',
             'EUC_NIR_W-CALIB-PSF_53402-Y-1_20170712T235523.5Z_00.00.fits',
             'EUC_NIR_W-CALIB-PSF_53402-J-3_20170712T235533.5Z_00.00.fits']

psfs_coords = [SkyCoord('5h23m34.5s', '-69d45m22s'),
              SkyCoord('5h23m34.5s', '-69d45m22s'),
              SkyCoord('5h23m34.5s', '-69d45m22s'),
              SkyCoord('5h23m34.5s', '-69d45m22s')]

nodes_coords = [SkyCoord('5h23m34.5s', '-69d45m22s'),
              SkyCoord('5h23m34.5s', '-69d45m22s'),
              SkyCoord('5h23m34.5s', '-69d45m22s'),
              SkyCoord('5h23m34.5s', '-69d45m22s')]

object_coords = SkyCoord('5h23m34.5s', '-69d45m22s')

def coordinate_to_distance(ra_psf, dec_psf, ra_object, dec_object):
    higher_dec = max(dec_object, dec_psf)   #prendre le plus haut dec : psf ou objet
    distance = np.sqrt( ( (ra_psf-ra_object)*cos(higher_dec) )**2 + (dec_psf-dec_object)**2 )
    return distance

def distance_to_weight(distance):
    weight = 1-distance
    return weight

def load_vis_cube(filename):
    header = pyfits.getheader(filename[0])
    naxis1 = header['NAXIS1']
    naxis2 = header['NAXIS2']
    nbr_psf = len(filename)
    psf_cube = np.zeros((nbr_psf, naxis1, naxis2), dtype=float)
    for i_psf in range(nbr_psf):
        psf_cube[i_psf] = pyfits.open(filenames[i_psf])[0].data
    return psf_cube, header


def simple_average(filename, weight=False, ra_psf=None, dec_psf=None, ra_object=None, dec_object=None):
    psf_cube, hdr = load_vis_cube(filename)
    if weight:
        distance = coordinate_to_distance(ra_psf, dec_psf, ra_object, dec_object)
        weights = distance_to_weight(distance)
    average = np.average(psf_cube, axis=0, weights=(0.5,0.5,0.5,0.5))
    return average

psf_cube, header = load_vis_cube(filenames)
psf_weighted_mean = simple_average(filenames)


for i in range(4):
    plt.figure(i)
    plt.imshow(np.log(psf_cube[i]))
    plt.colorbar()

plt.figure(5)
plt.imshow(np.log(psf_weighted_mean))   #, cmap='gray'
plt.colorbar()
plt.show()

"""







"""

# --- expand_psf_cube

# data = hdu[1].data

# TODO : to_broadband (3 options: nearest PSF, interpol splin et interpol linear). Avec N objet pour l'interpolation

# TODO : write_effective_psf


"""
filename = 'EUC_NIR_W-CALIB-PSF_53402-H-1_20170717T104421.7Z_00.00.fits'

#img, hdr = pyfits.getdata(filename, header=True)
#with pyfits.open(filename) as hdu/home/rperalta

#--- Lecture fichier
#Number of PSF in filename
hdu = pyfits.open(filename) #donne du fichier fits
header = hdu[0].header

total_number_psf = header['NEXTEND']    #nombre total d'image dans le fichier fits
number_psf_per_line = int(np.sqrt(total_number_psf))    #nombre total d'image dans le fichier fits

#--- mise en forme des données
#  -> sous forme de VECTEUR
vector = []
for i in range(1,total_number_psf+1):
    vector.append(hdu[i].data)
#print(vector)


#  -> sous forme de MATRICE
matrix = np.zeros(( number_psf_per_line, number_psf_per_line, len(hdu[1].data), len(hdu[1].data) ), dtype=float)
n_image=0
for Y in range(0,number_psf_per_line):
    for X in range(0,number_psf_per_line):
        n_image += 1
        matrix[Y][X] = (hdu[n_image].data)
#print(matrix)
"""
